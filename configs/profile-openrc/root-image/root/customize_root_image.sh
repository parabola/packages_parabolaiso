#!/bin/bash

set -e -u

check_dbus() {
  cat /etc/$1|grep dbus
}

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/

useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /usr/bin/zsh parabola

# Create the dbus user if it doesn't exist
if [[ $(check_dbus group) = "" ]]; then
  echo "Creating dbus group"
  groupadd -g 81 dbus
fi
if [[ $(check_dbus passwd) = "" ]]; then
  echo "Creating dbus user"
  useradd -r -s /sbin/nologin -g 81 -u 81 dbus
fi

chmod 750 /etc/sudoers.d
chmod 440 /etc/sudoers.d/g_wheel

sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist

rc-update add NetworkManager default
rc-update add dbus default
rc-update add haveged default
rc-update add pacman-init default

sed -i "s/_DATE_/${iso_version}/" /etc/motd
