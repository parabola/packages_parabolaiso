#!/bin/bash

#Comprobe if language was selected
code=$(cat ~/.codecheck | grep LANG= | tail --bytes 2)
if [ $code = "0" ]; then
	lang=$(dialog --stdout --backtitle "System language selection" --menu "Choose your language:" 15 40 10 \
                                                      "en_US.UTF-8" "English" \
                                                      "gl_ES.UTF-8" "Galego" \
                                                      "es_ES.UTF-8" "Spanish" \
                                                      "pt_BR.UTF-8" "Brazilian Portuguese" \
                                                      "pl_PL.UTF-8" "Polish" \
                                                      "it_IT.UTF-8" "Italian" \
                                                      "fr_FR.UTF-8" "French" \
                                                      "eo" "Esperanto")
	
	#Copy locale in locale.conf
	[ $lang = "" ] || echo "LANG=$lang" > /etc/locale.conf

	#Put a new line confirm that language was selected
	sed -i '/LANG=./d' ~/.codecheck
	echo "LANG=1" >> ~/.codecheck
	
	#Copy scripts to install in that language
	case ${lang/_*/} in
        en|gl|es|pt) cp -a ~/.scriptsInstallation/language/${lang/_*/}/* ~/.scriptsInstallation/
                     cp ~/.scriptsInstallation/install.sh ~/
                     ;;
                  *) cp -a ~/.scriptsInstallation/language/en/* ~/.scriptsInstallation/
                     cp ~/.scriptsInstallation/install.sh ~/
                     ;;
    esac
fi
