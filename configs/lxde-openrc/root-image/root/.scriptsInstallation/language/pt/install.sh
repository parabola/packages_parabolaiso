#!/bin/bash
#Temporal is a file that contains parameters to use when access to chroot

if [ ! -f ~/.scriptsInstallation/.pacman ]; then
    pacman -Sy parabola-keyring archlinux-keyring --noconfirm
    pacman-key --populate parabola archlinux
    pacman-key --refresh-keys
    case $? in
        0) touch ~/.scriptsInstallation/.pacman
        ;;
        *) false
        ;;
    esac
fi

partition(){
	
	#Search and show the Hard Disks to select
	aux=$(ls /dev/sd?)
	index=0
	for i in $aux; do
		hdds[$index]="${i} ${i#/*/} off"
		index=$((index+1))	
	done

	hdd=$(dialog --stdout --radiolist "Escolha o Disco" 20 70 50 ${hdds[@]})
        
	#If exist the line delete
	if (cat ~/.scriptsInstallation/temporal | grep "hdd=") &>/dev/null
	then
 		sed -i -e '/hdd=*/d' ~/.scriptsInstallation/temporal
	fi

	#And add the new line with new parameter
	echo "hdd=$hdd" >> ~/.scriptsInstallation/temporal
	
	selection=$(dialog --stdout --menu "Método de Particionamento"  20 70 50 \
			1 "Usar todo o disco com o swap (1GB) e /" \
			2 "Usar o gparted para personalizar" )

	case $selection in
		1)
			umount /mnt &> /dev/null
			#Create msdos partition table
			parted -s $hdd -- mklabel msdos

			#Create partition swap and /
			parted -s $hdd -- mkpart primary 1MiB 1000MiB mkpart primary 1000MiB -1s
                        
                        #Boot option partition /
                        parted -s $hdd -- set 2 boot on

			#Format and partitions and mount /
			(echo t; echo 1; echo 82; echo w) | fdisk $hdd
			mkswap ${hdd}1
			mkfs.ext4 ${hdd}2
			mount ${hdd}2 /mnt
			;;
		2)	
			gparted $hdd

			#Search and show the partitions that select before.
			aux=$(ls $hdd?)
			index=0;
			for i in $aux; do
				partitions[$index]="${i} ${i#/*/} off"
				index=$((index+1))
			done

			partition=$(dialog --stdout --radiolist "Montar a partição /" 20 70 50 ${partitions[@]})
	
			#Mount partition /
			umount /mnt &> /dev/null
			mount $partition /mnt

			#Ask if you want mount other partitions
			other=0
			while [ $other != 3 ]; do
				other=$(dialog --stdout --menu "Quer montar outra partição?" 20 70 50 1 "/home" 2 "/boot" 3 "Não")
	
				case $other in
				1)
					umount /mnt/home &> /dev/null
					mkdir /mnt/home &> /dev/null
					mount $(dialog --stdout --radiolist "Montar a partição /home" 20 70 50 ${partitions[@]}) /mnt/home
					;;
				2)
					umount /mnt/boot &> /dev/null	
					mkdir /mnt/boot &> /dev/null
					mount $(dialog --stdout --radiolist "Montar a partição /boot" 20 70 50 ${partitions[@]}) /mnt/boot
					;;
				*)	
					other=3
					;;
				esac
			done
			;;
		esac	
}


option=0
while [ option != 7 ]; do
	option=$(dialog --stdout --menu "Instalação CLI da Parabola"  20 70 50 \
			1 "Formatar e Montar as Partições" \
			2 "Instalar o Sistema Base" \
			3 "Instalar o GRUB" \
			4 "Configuração do Sistema" \
			5 "Adicionar uma Conta de Usuário" \
			6 "(Opcional) Instalar Ambiente Gráfico/Aplicativos da Live DVD" \
			7 "Sair" )

	case $option in
		1)
			partition
			;;
		2)	
			#Install base system
			pacstrap /mnt base-openrc
			pacstrap /mnt dialog
			;;
		3)
			#Install grub
			pacstrap /mnt grub
			;;
		4)
			#Generate fstab and acces to chroot to do System Config
			genfstab -p /mnt >> /mnt/etc/fstab
			cp ~/.scriptsInstallation/temporal /mnt
			cp ~/.scriptsInstallation/systemConfig.sh /mnt
			chmod +x /mnt/systemConfig.sh
			arch-chroot /mnt /systemConfig.sh	
			rm -r /mnt/systemConfig.sh  
			;;
		5)	
			#Create a new username and save in temporal, to use after
			if (cat ~/.scriptsInstallation/temporal | grep "userName=") &>/dev/null
			then	
				sed -i -e '/userName=*/d' ~/.scriptsInstallation/temporal
			fi

			echo "userName=$(dialog --stdout --inputbox "Insira o nome do usuário" 8 40)" >> ~/.scriptsInstallation/temporal
			cp ~/.scriptsInstallation/temporal /mnt
			cp ~/.scriptsInstallation/userAccount.sh /mnt
			chmod +x /mnt/userAccount.sh
			arch-chroot /mnt /userAccount.sh
			rm -r /mnt/userAccount.sh 
			;;
		6)
			#Packages to the X11 live
			packages=(	"xorg-server"
					"xf86-input-evdev"
					"xf86-input-synaptics"
					"xf86-video-ati"
					"xf86-video-dummy"
					"xf86-video-fbdev"
					"xf86-video-intel"
					"xf86-video-nouveau"
					"xf86-video-openchrome"
					"xf86-video-sisusb"
					"xf86-video-vesa"
					"xf86-video-vmware"
					"xf86-video-voodoo"
					"xf86-video-qxl"
					"xorg-xinit"
					"gst-plugins-good"
					"gst-libav"
					"lxde"
					"volumeicon"
					"zenity"
					"octopi"
					"pulseaudio-alsa"
					"alsa-utils"
					"networkmanager-elogind"
					"network-manager-applet"
					"ath9k-htc-firmware"
					"iceweasel"
					"icedove"
					"pidgin"
					"gparted"
					"smplayer"
					"epdfview"
					"gpicview"
					"abiword"
					"gnumeric"
					"leafpad"
					"galculator-gtk2"
					"xarchiver"
					"openrc-desktop"
					"polkit-elogind"
					"gvfs-mtp"
                    "gvfs-gphoto2"
                    "xdg-user-dirs"
                    "gnome-screenshot"
					)
			
			#Install packages			
			pacman -Sy -r /mnt ${packages[@]} --needed --noconfirm
			user=$(cat ~/.scriptsInstallation/temporal | grep "userName" )
			
			#Copy skel in the new system and desktop's background
			cp -a /etc/skel/ /mnt/etc/
			cp -a /etc/wallpaper.png /mnt/etc/wallpaper.png

			#Puts the XKBMAP, start X11 automatically and icewm desktop
			echo "setxkbmap $(cat ~/.codecheck | grep XKBMAP= | cut -d '=' -f 2)" > /mnt/etc/skel/.xinitrc
			echo "exec startlxde" >> /mnt/etc/skel/.xinitrc
			echo "startx" >> /mnt/etc/skel/.bash_profile
			chmod +x /mnt/etc/skel/.xinitrc
			cp -a /mnt/etc/skel/.[a-z]* /mnt/home/${user#*=}/
			cp -a ~/.scriptsInstallation/x11.sh /mnt

			#Enable services with OpenRC and configure other stuff
			chmod +x /mnt/x11.sh
			arch-chroot /mnt /x11.sh 
			rm /mnt/x11.sh
			;;
		*)
			#Delete temporal file and umount partitions
			rm -r /mnt/temporal
			umount /mnt/boot &> /dev/null
			umount /mnt/home &> /dev/null
			umount /mnt &> /dev/null		
			exit
			;;
	esac
done
