#!/bin/bash

user=$(cat /temporal | grep "userName")

password=$(dialog --stdout --passwordbox "Insira a senha para o usuário ${user#*=}" 8 40)

while [[ $password != $password2 ]]; do
	password2=$(dialog --stdout --passwordbox "Repita a senha" 8 40) 
done

useradd -m -g users -G "wheel" -s /bin/bash -p $(openssl passwd $password) ${user#*=}

exit
