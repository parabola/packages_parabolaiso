#!/bin/bash

user=$(cat /temporal | grep "userName")

password=$(dialog --stdout --passwordbox "Introduce o contrasinal para o usuario ${user#*=}" 8 40)

while [[ $password != $password2 ]]; do
	password2=$(dialog --stdout --passwordbox "Repite o contrasinal" 8 40) 
done

useradd -m -g users -G "wheel" -s /bin/bash -p $(openssl passwd $password) ${user#*=}

exit
