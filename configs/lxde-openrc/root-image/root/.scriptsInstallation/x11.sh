#!/bin/bash

check_dbus() {
  grep dbus /etc/$1
}

# Create the dbus user if it doesn't exist
[[ $(check_dbus group) = "" ]] && groupadd -g 81 dbus
[[ $(check_dbus passwd) = "" ]] && useradd -r -s /sbin/nologin -g 81 -u 81 dbus

# Enable services with OpenRC
rc-update add NetworkManager default
rc-update add dbus default
rc-update add alsasound default
