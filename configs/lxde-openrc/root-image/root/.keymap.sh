#!/bin/bash

list_keymaps() {
cat <<EOM
af Afrikaans
al Albanian
am Amharic
ara Arabic 
at at 
au au 
az Azerbaijani
ba Bashkir
be Belarusian
bg Bulgarian
bn Bihari-languages
br Breton
brai brai 
bt bt 
bw bw 
by by 
ca Catalan
cd cd
ch Chamorro
cm cm 
cn cn
cz Czech
de German
dk dk 
dz Dzongkha
ee Ewe
epo Esperanto
es Spanish
et Estonian
fi Finnish
fo Faroese
fr French
gb gb 
ge ge
gh gh
gn Guaraní
gr gr 
hr Croatian
hu Hungarian
id Indonesian
ie Interlingue
il il 
in in 
iq iq 
ir ir 
is Icelandic
it Italian
jp Japanese
ke ke 
kg Kongo
kh kh 
kr Kanuri
kz kz 
la Latin
latam Latin-American
lk lk 
lt Lithuanian
lv Latvian
ma ma
mao Maori
md md 
me me
mk Macedonian
ml Malayalam
mm mm 
mn Mongolian
mt Maltese
mv mv 
my Burmese
nec_vndr/jp nec_vndr/jp
ng Ndonga
nl Dutch
no Norwegian
np np 
ph ph 
pk pk
pl Polish
pt Portuguese
ro Romanian
rs rs
ru Russian
se Northern-Sami
si Sinhala
sk Slovak
sn Shona
sy sy 
tg Tajik
th Thai
tj tj
tm tm
tr Turkish
tw Twi
tz tz
ua ua
us English
uz Uzbek
vn vn
za Zhuang
EOM
}

#Comprobe if X11 keymap was selected
code=$(cat .codecheck | grep XKBMAP= | cut -d '=' -f 2)
if [[ $code = "0" ]]; then
	keymap=$(zenity --list --title="Select your keymap" --column="Code Name" --column="Keymap" --hide-column=1 $(list_keymaps))
	
	setxkbmap $keymap
	
	#Save XKBMAP in .codecheck to use in other time. For example if you install X11 with scripts
	sed -i '/XKBMAP=./d' ~/.codecheck
	echo "XKBMAP=$keymap" >> ~/.codecheck
fi
