#!/bin/bash

set -e -u

check_dbus() {
  grep dbus /etc/$1
}

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(es_ES\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(gl_ES\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(pt_BR\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(pl_PL\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(it_IT\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(fr_FR\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(eo\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/

useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel,disk,sys" -s /usr/bin/zsh parabola

# Create the dbus user if it doesn't exist
[[ $(check_dbus group) = "" ]] && groupadd -g 81 dbus
[[ $(check_dbus passwd) = "" ]] && useradd -r -s /sbin/nologin -g 81 -u 81 dbus

chmod 750 /etc/sudoers.d
chmod 440 /etc/sudoers.d/g_wheel

sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist

rc-update add NetworkManager default
rc-update add elogind default
rc-update add alsasound default
rc-update add dbus default
rc-update add haveged default
rc-update add pacman-init default

sed -i "s/_DATE_/${iso_version}/" /etc/motd
